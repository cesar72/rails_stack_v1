#
# Cookbook:: rails_stack
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

apt_package 'nodejs' do
  action :install
end

apt_package 'imagemagick' do
  action :install
end

apt_package 'zip' do
  action :install
end

apt_package 'libmagickwand-dev' do
  action :install
end

apt_package 'sshfs' do
  action :install
end

apt_package 'openjdk-7-jdk' do
  action :install
end

apt_package 'libmysqlclient-dev' do
  action :install
end

apt_package 'mysql-client-5.5' do
  action :install
end
