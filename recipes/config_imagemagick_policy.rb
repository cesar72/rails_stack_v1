ruby_block 'comment_policy_not_alow_read_write_pdf_in_policy.xml' do
  block do
    regex = /<policy domain="coder" rights="none" pattern="PDF" \/>/
    file = Chef::Util::FileEdit.new('/etc/ImageMagick/policy.xml')
    file.search_file_replace_line(regex,'<!-- <policy domain="coder" rights="none" pattern="PDF" /> -->')
    file.write_file
  end
  only_if { File.exist?('/etc/ImageMagick/policy.xml') }
end
