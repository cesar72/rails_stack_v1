directory '/var/www/remote-storage' do
  owner 'deploy'
  group 'www-data'
  mode '0755'
  action :create
  only_if {!Dir.exists?('/var/www/remote-storage/lms-storage')}
end

group 'fuse' do
  action :modify
  members 'deploy'
  append true
end

file '/etc/fuse.conf' do
  mode '0644'
end

ruby_block 'uncomment_user_allow_others_fuse.conf' do
  block do
    regex = /\#user_allow_other$/
    file = Chef::Util::FileEdit.new('/etc/fuse.conf')
    file.search_file_replace_line(regex,'user_allow_other')
    file.write_file
  end
  only_if { File.exist?('/etc/fuse.conf') }
end
